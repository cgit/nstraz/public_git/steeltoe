%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

Name:           steeltoe
Version:        0.9.7
Release:        1
Summary:        Steel Toe - Kickstart automation tools

Group:          QA
License:        GPL
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0:        steeltoe-%{version}.tar.bz2
Requires:	/usr/bin/xsltproc tftp-server dhcp gxpp
ExclusiveArch: noarch


%description
Steel Toe is a set of tools to help automate the generation of 
Kickstart and netboot config files.

%package django
Summary: Steel Toe Web Interface
Requires: Django >= 0.96
Group: QA

%description django
This is a web interface for Steel Toe.

%prep
%setup -q -c


%build
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc README DESIGN
/usr/bin/steeltoe
%config(noreplace) /etc/steeltoe
%config /etc/httpd/conf.d/tftp.conf

%files django
%defattr(-,root,root,-)
%doc st_web/README
%dir %{python_sitelib}/st_web
%{python_sitelib}/st_web/


%changelog
* Tue Oct 07 2008 Nate Straz <nstraz@redhat.com> 0.9.7-1
- Remove the virtinstall requirement of a shared device.

* Tue Oct 7 2008  0.9.6-1
- Handle the case where we can't figure out the volumes on a dom0.

* Tue Jul 22 2008  0.9.5-1
- Ping the host before using $RSH to contact it.

* Fri Jun 27 2008  0.9.4-1 
- Add virtinstall command

* Fri Jun 13 2008  0.9.2-2
- Add recently used forms for trees and hosts.

* Thu Jun 12 2008 Nate Straz <nstraz@redhat.com> 0.9.2-1
- Add st_web Django application and give it the teeth to do installs

* Wed Feb 13 2008  0.9-1
- Add groups

* Thu Jan 10 2008  0.8-1
- New virtinstall command

* Tue Nov 27 2007  0.7-1
- Add an option to override the arch of a host.

* Tue Jul 10 2007  0.6-3
- Fix bz 214874

* Mon Apr 16 2007  0.6-2 
- Try to get ppc support correct

* Wed Jan 31 2007  0.6-1 
- Add preliminary ppc support

* Wed Sep 13 2006  0.5-1 
- st_xpe and st_xpm were moved to their own package, xpe.

* Mon Jun 05 2006  0.3-1 
- Add support for EFI
- Merge up a bunch of functional changes into a package.

* Tue May 16 2006 steeltoe 0.2-1 
- Add st_xpm utility

* Tue Apr 04 2006 steeltoe 0.1-1
- Initial packaging

