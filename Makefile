
PYTHON_SITELIB := $(shell python -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")

TARGETS := steeltoe

all: $(TARGETS) build-python

build-python: setup.py
	python setup.py build

XSL := $(wildcard *.xsl)

install: all
	install -D steeltoe $(DESTDIR)/usr/bin/steeltoe
	install -D tftp.conf $(DESTDIR)/etc/httpd/conf.d/tftp.conf
	install -D steeltoe.xml $(DESTDIR)/etc/steeltoe/steeltoe.xml
	for i in $(XSL); do install -D $$i $(DESTDIR)/etc/steeltoe/$$i; done
	install -d $(DESTDIR)$(PYTHON_SITELIB)/st_web/
	python setup.py install --skip-build --root $(DESTDIR)
	python setup.py install -O1 --skip-build --root $(DESTDIR)
	cp -R st_web/templates $(DESTDIR)$(PYTHON_SITELIB)/st_web/

clean:
	$(RM) $(TARGETS)


VERSION := $(shell awk '/^Version:/ { print $$2 }' steeltoe.spec)
steeltoe-$(VERSION).tar.bz2: steeltoe.spec Makefile
	-$(RM) $@
	tar cjf $@ --exclude=.svn --exclude=*.pyc .

tarball: steeltoe-$(VERSION).tar.bz2

rpm: steeltoe-$(VERSION).tar.bz2 steeltoe.spec
	rpmbuild --target=noarch -ta $<

srpm: steeltoe-$(VERSION).tar.bz2 steeltoe.spec
	rpmbuild -ts $<
