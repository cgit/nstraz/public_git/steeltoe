<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                version="1.0">
	<xsl:variable name="ks-rft">
         <xsl:choose>
	  <xsl:when test="$host/kickstart/@family = $dist/meta/family"><xsl:copy-of select="//host[@name=$hostname]/kickstart[@family=$dist/meta/family]"/></xsl:when>
	  <xsl:otherwise><xsl:copy-of select="//host[@name=$hostname]/kickstart[count(@*)=0]"/></xsl:otherwise>
         </xsl:choose>
	</xsl:variable>
	<xsl:variable name="ks" select="exsl:node-set($ks-rft)/kickstart"/>
</xsl:stylesheet>
