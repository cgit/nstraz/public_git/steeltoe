<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
		version="1.0">
	<xsl:param name="hostname"/>
	<xsl:param name="treename"/>
	<xsl:variable name="host" select="//host[@name=$hostname]"/>
	<xsl:variable name="dist" select="//tree[@name=$treename and meta/arch=$host/arch]"/>
	<xsl:include href="select-ks.xsl"/>
	<xsl:output method="text" indent="no"/>
	<xsl:template match="/" xml:space="preserve">
prompt  
timeout=50
relocatable

image=vmlinuz
        label=linux
        initrd=initrd.img
        read-only
        append="<xsl:if test="$host/console">console=<xsl:value-of select="$host/console"/></xsl:if> ks=<xsl:value-of select="/steeltoe/config/kickstartbase"/>/<xsl:value-of select="$hostname"/>/ks.cfg ksdevice=<xsl:value-of select="$ks/ksdevice"/> <xsl:value-of select="$ks/ksparams"/> "
</xsl:template>
</xsl:stylesheet>
