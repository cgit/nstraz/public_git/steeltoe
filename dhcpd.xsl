<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" indent="no"/>
	<xsl:template match="/">
		<xsl:apply-templates select="//host" />
	</xsl:template>
	<xsl:template match="//host">
host <xsl:value-of select="@name" xml:space="preserve"/> {
	hardware ethernet <xsl:value-of select="macaddr"/>;
	fixed-address <xsl:value-of select="@name"/>;
	option host-name "<xsl:value-of select="@name"/>";
	<xsl:if test="bootloader">
	filename "<xsl:value-of select="/steeltoe/config/tftpbase"/>/<xsl:value-of select="@name"/>/<xsl:value-of select="bootloader"/>";
	next-server <xsl:value-of select="/steeltoe/config/tftpserver"/>;
	</xsl:if>
}
</xsl:template>
</xsl:stylesheet>
