<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:str="http://exslt.org/strings"
                version="1.0">
	<xsl:param name="class"/>
	<xsl:param name="verbose" select="0"/>
	<xsl:output method="text"/>
	<xsl:template match="/steeltoe">
		<xsl:choose>
		<xsl:when test="$class = 'host'">
		<xsl:apply-templates select="//host" />
		</xsl:when>
		<xsl:when test="$class = 'group'">
		<xsl:apply-templates select="//group" />
		</xsl:when>
		<xsl:when test="$class = 'tree'">
		<xsl:apply-templates select="//tree" />
		</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- INSTALLTREE TEMPLATE -->
	<xsl:template match="tree" xml:space="preserve"><xsl:value-of select="str:align(@name, '               ', 'left')" /> <xsl:value-of select="path"/><xsl:if test="$verbose">
    <xsl:apply-templates select="meta"/></xsl:if>
</xsl:template>

	<!-- METADATA TEMPLATE FOR INSTALLTREE -->
   	<xsl:template match="meta"><xsl:for-each select="*" xml:space="preserve"><xsl:value-of select="name()"/>: <xsl:value-of select="." /> </xsl:for-each></xsl:template>

	<!-- HOST TEMPLATE -->
	<xsl:template match="host" xml:space="preserve"><xsl:value-of select="str:align(@name, '               ', 'left')" /><xsl:value-of select="str:align(fqdn, '                                                  ','left')"/><xsl:value-of select="str:align(arch,'        ', 'right')"/>
</xsl:template>

	<!-- GROUP TEMPLATE -->
	<xsl:template match="group" xml:space="preserve"><xsl:value-of select="str:align(@name, '               ', 'left')"/><xsl:value-of select="."/>
</xsl:template>
</xsl:stylesheet>
