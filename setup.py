from distutils.core import setup

setup(name='steeltoe',
	version="0.9.1",
	description="Steel Toe - Kickstart automation tool",
	author="Nate Straz",
	author_email="nstraz@redhat.com",
	packages=["st_web", "st_web.templatetags"],
	)
