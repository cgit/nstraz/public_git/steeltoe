from django.conf.urls.defaults import *

urlpatterns = patterns('',
    # Example:
    (r'^$', 'django.views.generic.simple.direct_to_template', {'template': 'st_web/index.html'}),
    (r'^install/$', 'st_web.views.install'),
    (r'^virtinstall/$', 'st_web.views.virtinstall'),

    # Uncomment this for admin:
#     (r'^admin/', include('django.contrib.admin.urls')),
)
