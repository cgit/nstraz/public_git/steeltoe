# Main views for the Steeltoe Web Interface.

from sets import Set
from commands import getoutput

from django.http import *
from django.template import RequestContext
from django.shortcuts import render_to_response
from django import newforms as forms
from django.newforms.util import ErrorList
from django.utils.datastructures import SortedDict

from st_web.st_config import steeltoe
from st_web.widgets import VolSelect

def steeltoe_install_form():
	"Generate a validatable form with choices from the steeltoe config"
	def _get_trees():
		return tuple([(x, x) for x in steeltoe.treenames])
	def _get_hosts():
		return tuple([(x, x) for x in steeltoe.hostnames | steeltoe.groupnames])

	class SteeltoeInstallForm(forms.Form):
		tree = forms.ChoiceField(choices = _get_trees(), widget=forms.RadioSelect)
		hosts = forms.MultipleChoiceField(choices = _get_hosts(), widget=forms.CheckboxSelectMultiple)

		def clean(self):
			"Test that the arch of the hosts matches the archs of the tree"
			errors = ErrorList()
			t = self.clean_data.get('tree', None)
			if t is None:
				return self.clean_data
			tree_archs = [x['meta']['arch'] for x in steeltoe.trees if x['name'] == t]
			# Normalize the list into hostnames
			hostlist = Set()
			for host in self.clean_data.get('hosts', []):
				if host in steeltoe.groupnames:
					hostlist |= Set(steeltoe.get_group(host)['members'])
				elif host in steeltoe.hostnames:
					hostlist.add(host)
				else:
					errors.append("Host %s not valid" % host)
			# Convert the list to host entries
			hostlist = list(hostlist)
			hostlist.sort()
			self.clean_data['hosts'] = hostlist
			hostlist = [steeltoe.get_host(x) for x in hostlist]
			# Do the tests
			for host in hostlist:
				if not host['arch'] in tree_archs:
					errors.append("Tree %s is not available for %s which host %s requires" % (t, host['arch'], host['name']))
			if errors:
				raise forms.ValidationError(errors)
			return self.clean_data

	return SteeltoeInstallForm

def install(request):
	if request.method == 'POST':
		form = steeltoe_install_form()(request.POST)
		if form.is_valid():
			cmd = request.POST.get('install', 'Install')
			if cmd == 'Install':
				return render_to_response("st_web/confirm.html", dict(form.clean_data, form=form),
					context_instance=RequestContext(request))
			elif cmd == 'Confirm':
				t = form.clean_data['tree']
				h = ' '.join(form.clean_data['hosts'])
				update_session(request.session, t, form.clean_data['hosts'])
				output = getoutput("/usr/bin/steeltoe install -r %s %s" % (t, h))
				return render_to_response("st_web/perform.html", {'output': output},
					context_instance=RequestContext(request))
	else:
		steeltoe.refresh()
		form = steeltoe_install_form()()
	return render_to_response("st_web/install.html", { 'steeltoe': steeltoe , 'form': form },
			context_instance=RequestContext(request))

def steeltoe_virthost_form():
	"Generate a validatable form with choices from the steeltoe config"
	def _get_trees():
		return tuple([(x, x) for x in steeltoe.treenames])
	def _get_dom0s():
		return tuple([(x, x) for x in steeltoe.hostnames])
	def _get_hosts():
		return tuple([(x, x) for x in steeltoe.hostnames | steeltoe.groupnames])

	class SteeltoeVirtHostForm(forms.Form):
		tree = forms.ChoiceField(choices = _get_trees(), widget=forms.RadioSelect)
		dom0 = forms.ChoiceField(choices = _get_dom0s(), widget=forms.RadioSelect)
		hosts = forms.MultipleChoiceField(choices = _get_hosts(), widget=forms.CheckboxSelectMultiple)

		def clean(self):
			"Test that the arch of the hosts matches the archs of the tree"
			errors = ErrorList()
			t = self.clean_data.get('tree', None)
			if t is None:
				return self.clean_data
			tree_archs = [x['meta']['arch'] for x in steeltoe.trees if x['name'] == t]
			d0 = self.clean_data.get('dom0', None)
			if d0 is None:
				return self.clean_data
			# Normalize the list into hostnames
			hostlist = Set()
			for host in self.clean_data.get('hosts', []):
				if host in steeltoe.groupnames:
					hostlist |= Set(steeltoe.get_group(host)['members'])
				elif host in steeltoe.hostnames:
					hostlist.add(host)
				else:
					errors.append("Host %s not valid" % host)
			# Convert the list to host entries
			hostlist = list(hostlist)
			hostlist.sort()
			if d0 in hostlist:
				errors.append("Can not install on %s from %s" % (d0, d0))
			self.clean_data['hosts'] = hostlist
			hostlist = [steeltoe.get_host(x) for x in hostlist]
			# Do the tests
			for host in hostlist:
				if not host['arch'] in tree_archs:
					errors.append("Tree %s is not available for %s which host %s requires" % (t, host['arch'], host['name']))
			if errors:
				raise forms.ValidationError(errors)
			return self.clean_data

	return SteeltoeVirtHostForm


def steeltoe_virtvols_form(dom0, domUs):
	"Generate a validatable form for the volumes which we're going to install the hosts to"
	def _get_vols(host):
		rsh = steeltoe.config.get("remoteshell", "/bin/false")
		output = getoutput("%s -l root %s /usr/sbin/lvs --noheadings -o lv_name,vg_name,lv_size,lv_attr 2>/dev/null" % (rsh, host))
		vols = []
		for l in output.split('\n'):
			if len(l.split()) != 4:
				continue
			d = dict(zip(('lv', 'vg', 'size', 'attr'), l.split()))
			# skip over open volumes
			if d['attr'][5] == 'o':
				continue
			vols.append(d)
		return vols

	def clean(form):
		errors = ErrorList()
		# Check that each host has one and only one root volume
		hostroots = dict([(h, []) for h in form.hosts])
		shared = []
		for vol in form.fields:
			host = form.clean_data[vol]
			if host == 'none':
				continue
			elif host == 'shared':
				shared.append(vol)
				continue
			elif not hostroots.has_key(host):
				errors.append("Unexpected host " + host)
			else:
				hostroots[host].append(vol)
		for host, roots in hostroots.items():
			if len(roots) > 1:
				errors.append("Too many roots assigned to %s" % host)
			elif len(roots) == 0:
				errors.append("Root not assigned to %s" % host)
		form.clean_data['shared'] = [{'lv': form.fields[v].lv, 'vg': form.fields[v].vg, 'size': form.fields[v].size} for v in shared]
		if errors:
			raise forms.ValidationError(errors)
		# Add an easily usable dict to look hosts' root vol
		form.clean_data['hosts'] = [({'name': k, 'lv': form.fields[v[0]].lv, 'vg': form.fields[v[0]].vg, 'size': form.fields[v[0]].size}) for k, v in hostroots.items()]
		return form.clean_data

	vols = _get_vols(dom0)
	choices = tuple([(h, 'Root for %s' % h) for h in domUs]) + (('shared', 'Shared Volume'), ('none', 'Not Used'))

	base_fields = SortedDict()
	for vol in vols:
		f = forms.ChoiceField(choices, widget=VolSelect, initial='none')
		for k, v in vol.items():
			setattr(f, k, v)
		base_fields["%(vg)s_%(lv)s" % vol] = f

	return type('SteeltoeVirtVolForm', (forms.BaseForm,),
			{'base_fields': base_fields,
			 'hosts': domUs,
			 'dom0': dom0,
			 'vols': vols,
			 'clean': clean,
			})


def virtinstall(request):
	if request.method == 'POST':
		# Any POST request should have a host form to validate.
		hostform = steeltoe_virthost_form()(request.POST)
		if hostform.is_valid():
			step = request.POST.get('step', 'hosts')
			cmd = request.POST.get('install', 'Next')
			dom0 = hostform.clean_data['dom0']
			hosts = hostform.clean_data['hosts']
			if step == 'vols' and cmd == 'Cancel':
				return render_to_response("st_web/virthosts.html", 
					{ 'steeltoe': steeltoe , 'hostform': hostform },
					context_instance=RequestContext(request))
				
			if step in ('vols', 'confirm'):
				volform = steeltoe_virtvols_form(dom0, hosts)(request.POST)
				if volform.is_valid():
					if step == 'vols' and cmd == 'Install':
						data = dict(hostform.clean_data)
						data.update(volform.clean_data)
						data['hostform'] = hostform
						data['volform'] = volform
						return render_to_response("st_web/virtconfirm.html", data,
							context_instance=RequestContext(request))
					elif step == 'confirm' and cmd == 'Confirm':
						t = hostform.clean_data['tree']
						update_session(request.session, t, hostform.clean_data['hosts'])
						output = ""
						for h in volform.clean_data['hosts']:
							cmdargs = ("/usr/bin/steeltoe", "virtinstall")
							cmdargs += (t, h['name'], dom0)
							cmdargs += "/dev/%(vg)s/%(lv)s" % h,
							cmdargs += tuple(["/dev/%(vg)s/%(lv)s" % v for v in volform.clean_data['shared']])
							output += getoutput(' '.join(cmdargs))
						return render_to_response("st_web/virtperform.html", {'output': output},
							context_instance=RequestContext(request))
					# else: fall through to redisplay the volumes form
				# else: fall through to redisplay the volumes form with errors
			else:
				volform = steeltoe_virtvols_form(dom0, hosts)()
			# FIXME: What about back button for volumes -> hosts
			return render_to_response("st_web/virtvolumes.html",
				dict(hostform=hostform, volform=volform),
				context_instance=RequestContext(request))
		# else: host form is not valid, fall through to the first page
	else:
		steeltoe.refresh()
		hostform = steeltoe_virthost_form()()
	return render_to_response("st_web/virthosts.html", { 'steeltoe': steeltoe , 'hostform': hostform },
			context_instance=RequestContext(request))


def update_session(session, newtree, newhosts):
	trees = session.get('trees', [])
	if newtree in Set([x['name'] for x in trees]):
		tree = [x for x in trees if x['name'] == newtree][0]
		tree['count'] += 1
	else:
		trees.append({'name': newtree, 'count': 1})
	trees.sort(lambda x, y: cmp(y['count'], x['count']) or cmp(x['name'], y['name']))
	session['trees'] = [x for x in trees if x['name'] in steeltoe.treenames]
	hosts = session.get('hosts', [])
	for newhost in newhosts:
		if newhost in Set([x['name'] for x in hosts]):
			host = [x for x in hosts if x['name'] == newhost][0]
			host['count'] += 1
		else:
			hosts.append({'name': newhost, 'count': 1})
	hosts.sort(lambda x, y: cmp(y['count'], x['count']) or cmp(x['name'], y['name']))
	session['hosts'] = [x for x in hosts if x['name'] in steeltoe.hostnames]
