from django.template import resolve_variable
from django.template import Node, NodeList
from django.template import VariableDoesNotExist
from django.template import Library

register = Library()

class IfInNode(Node):
    def __init__(self, var1, var2, nodelist_true, nodelist_false, negate):
        self.var1, self.var2 = var1, var2
        self.nodelist_true, self.nodelist_false = nodelist_true, nodelist_false
        self.negate = negate

    def __repr__(self):
        return "<IfInNode>"

    def render(self, context):
        try:
            val1 = resolve_variable(self.var1, context)
        except VariableDoesNotExist:
            val1 = None
        try:
            val2 = resolve_variable(self.var2, context)
        except VariableDoesNotExist:
            val2 = []
	try:
            if (self.negate and not val1 in val2) or (not self.negate and val1 in val2):
                return self.nodelist_true.render(context)
        except TypeError:
		return ""
        return self.nodelist_false.render(context)

def do_ifin(parser, token, negate):
    bits = list(token.split_contents())
    if len(bits) != 3:
        raise TemplateSyntaxError, "%r takes two arguments" % bits[0]
    end_tag = 'end' + bits[0]
    nodelist_true = parser.parse(('else', end_tag))
    token = parser.next_token()
    if token.contents == 'else':
        nodelist_false = parser.parse((end_tag,))
        parser.delete_first_token()
    else:
        nodelist_false = NodeList()
    return IfInNode(bits[1], bits[2], nodelist_true, nodelist_false, negate)

#@register.tag
def ifin(parser, token):
    """
    Output the contents of the block if the first argument is in the second argument.

    Examples::

        {% ifin user.id comment.user_id %}
            ...
        {% endifin %}

        {% ifnotin user.id comment.user_id %}
            ...
        {% else %}
            ...
        {% endifnotin %}
    """
    return do_ifin(parser, token, False)
ifin = register.tag(ifin)

#@register.tag
def ifnotin(parser, token):
    """Output the contents of the block if the first argument is not in the second argument. See ifin."""
    return do_ifin(parser, token, True)

ifnotin = register.tag(ifnotin)
